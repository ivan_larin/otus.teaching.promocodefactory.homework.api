﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Schema.Models
{
    public class VoidType
    {
        public string Void { get; }
    }
}
